function clone(obj: object): object {
    const resultObj: string = JSON.stringify(obj);
    return JSON.parse(resultObj);
}


export default clone;