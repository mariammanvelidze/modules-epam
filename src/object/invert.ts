function invert(obj: object) {
    const resultObject: object = {};
    for(let i in obj) {
        resultObject[obj[i]] = Number(i) ? Number(i) : i;
    }

    return resultObject;
}

export default invert;