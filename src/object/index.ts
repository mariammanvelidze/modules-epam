import clone from './clone';
import invert from './invert';

export {invert, clone};