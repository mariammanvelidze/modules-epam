/* DO NOT CHANGE THIS FILE */
import * as arrayHelpers from './array'
const objectHelpers = require('./object');

export {
    arrayHelpers,  // {flatten: function() {...}, take: : function() {...}}
    objectHelpers, // {clone: function() {...}, invert: : function() {...}}
};
