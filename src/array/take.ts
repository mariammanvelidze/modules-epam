function take<T>(arr: T[], amount: number): T[] {
    return arr.splice(0,amount);
}

export default take;